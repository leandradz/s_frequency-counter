document.getElementById("countButton").onclick = function() {
  let lettersDiv = document.getElementById("lettersDiv")
  lettersDiv.innerHTML = ''
  let wordsDiv = document.getElementById("wordsDiv")
  wordsDiv.innerHTML = ''

  let typedText = document.getElementById("textInput").value;
  typedText = typedText.toLowerCase(); 
  typedText = typedText.replace(/[^a-z'\s]+/g, ""); 
  
  //contando letras
  const letterCounts = {};
  for (let i = 0; i < typedText.length; i++) {
    let currentLetter = typedText[i];
    
    if (letterCounts[currentLetter] === undefined) {
      letterCounts[currentLetter] = 1; 
    } else { 
      letterCounts[currentLetter]++; 
    }
  }
  
  for (let letter in letterCounts) { 
    const span1 = document.createElement("span");
    span1.innerText = '"' + letter + "\": " + letterCounts[letter] + ", "
    lettersDiv.appendChild(span1); 
  }
  
  //contando palavras
  let wordsArray = typedText.split(/\s/);
  const wordsCounts = {}
  for (let i = 0; i < wordsArray.length; i++) {
    let currentWords = wordsArray[i]
    
    if (wordsCounts[currentWords] === undefined) {
      wordsCounts[currentWords] = 1;
    } else {
      wordsCounts[currentWords]++;
    }
  }
  
  for (let words in wordsCounts) {
    const span = document.createElement('span')
    span.innerText = `"${words} : ${wordsCounts[words]}", `
    wordsDiv.appendChild(span)
  }
}
